import { ContactComponent } from './components/contact/contact.component';

import { SingleComponent } from './components/single/single.component';
import { BlogComponent } from './components/blog/blog.component';
import { WorkComponent } from './components/work/work.component';

import { ServiceComponent } from './components/service/service.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AboutComponent} from './components/about/about.component' ;
import {HomeComponent} from './components/home/home.component' ;




const approutes: Routes = [
  { path: 'about', component: AboutComponent},
  
  {path: 'service', component: ServiceComponent},
  {path: 'work', component: WorkComponent},
  {path: 'blog', component:BlogComponent},
  {path: 'single', component:SingleComponent},
  {path: 'contact', component:ContactComponent},
  {path: 'home', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(approutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
